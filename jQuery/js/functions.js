
$(document).ready(function(){
   
    $("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
      
 
  });
   
//    $('#wrapper').click(function(e) {
//        console.log(this, e.target);
//        if (this == e.target) {
//            alert('Kliknięto bezpośrednio na dużym divie');
//        }
//        else {
//            return false;
//        }
//    });
   
  

    $('#slidingBox a').click(function(e){
        if ($(this).hasClass('clicked')) {
            $(this).text('Zapytaj jeszcze raz').removeClass('clicked');
        }
        else {
            $(this).text('Na pewno?').addClass('clicked');
            e.preventDefault();
        }
    });
    
    
    
    $('#rightColumn a').click(function(e){
        
        console.log(e);
        
        if ($(this).hasClass('active')) {
            
            $(this).removeClass('active').text('Kliknij mnie');
            
            $('.column').css('float', 'left');
            
            $('#rightColumn').css({'margin-left': 20, 'margin-right': 0});
            
        }
        
        else {
             
            $(this).addClass('active').text('Powrót do przyszłości');
            
            $('.column').css('float', 'right');
            
            $('#rightColumn').css({'margin-left': 0, 'margin-right': 20});
            
        }
        
    });
    
    
    
    
    $('#slidingBox .label').click(function(){
        
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).parent().animate({'right': -248}, 1000);
        }
        
        else {
            $(this).addClass('open');
            $(this).parent().animate({'right': 0}, 1000);
        }
        
    });
    
    
    
    $('.contactForm form').submit(function(e){
        
        e.preventDefault();
        
        
        $(this).children().find('textarea, input').each(function(){
            if ($(this).attr('type') != 'submit') {
                alert('Nie wypełniłeś pola ' + $(this).attr('placeholder'));
                $(this).addClass('error');
            }
        });
        
        
    });
    

//    $('ul li a').click(function(){
//        
//        $(this).attr('target', '_blank');
//      
//    })
    

    $('#menu ul > li > a').click(function(e){
        
        if ($(this).next('ul').length > 0){
            e.preventDefault();
        };
      

     
        if($(this).hasClass('open')){
            $(this).removeClass('open');
            $(this).next('ul').slideUp() ;
            
        }else{
            $(this).addClass('open');
            $(this).next('ul').slideDown() ;
            
        }

      
    ;})

// $('#menu ul > li > a').click(function(e){
//        if ($(this).next('ul').length > 0) {
//            e.preventDefault();
//            $(this).hasClass('open') ? $(this).removeClass('open').next('ul').slideUp() : $(this).addClass('open').next('ul').slideDown();
//        }
//    });
//    

                $('ul.tabs li a').click(function(){
                    
                    var tab_id = $(this).attr('data-rel');
                    
                    $('ul.tabs li').removeClass('active');
                    $(this).parent().addClass('active');
                
                    $('div.tab').each(function(){
                       if ($(this).attr('id') == tab_id){
                         $(this).addClass('active');
                       }
                       else{
                         $(this).removeClass('active');
                           
                       }
                 
               
                 })
  
	})

             $('.contactForm form').validate({
        
        rules : {
            "contact[subject]" : {required : true, minlength: 3 },
            "contact[name]" : {required : true, minlength: 3},
            "contact[email]" : {required : true, email : true},
            "contact[content]" : { required : true, minlength: 20}
        },
        
        messages : {
            "contact[subject]" : "Wypełnij to pole!",
            "contact[name]" : "to też!",
            "contact[email]" : "i to też!",
            "contact[content]" : "no i to!"
        }
        
    });

 ;})
   