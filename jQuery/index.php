<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    
    <head>
        
        <meta charset="UTF-8" />
        
        <title>jQuery</title>
        
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        
        <link rel="stylesheet" href="js/owl/owl.carousel.css" type="text/css" />
        <link rel="stylesheet" href="js/owl/owl.theme.css" type="text/css" />
        
        
        <script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
        
        <script type="text/javascript" src="js/owl/owl.carousel.js"></script>

        <script type="text/javascript" src="js/jquery.validate.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
<!--         <script>
             $(".body-img").height($(window).height());

             $(window).resize(function(){
             $(".body-img").height($(window).height());
             });
        </script> -->
        
    </head>
    
    
    
    <body>
        
        
        <div id="wrapper">
            
            
            <div id="header" class="container">
                
                <div id="menu">
                    <ul>
                        <li>
                            <a href="http://google.pl">Element 1</a>
                            <ul>
                                <li><a href="#">PodElement 1</a></li>
                                <li><a href="#">PodElement 2</a></li>
                                <li><a href="#">PodElement 3</a></li>
                                <li><a href="#">PodElement 4</a></li>
                                <li><a href="#">PodElement 5</a></li>
                            </ul>
                        </li>
                        <li><a href="http://google.pl">Element 2</a></li>
                        <li>
                            <a href="http://google.pl">Element 3</a>
                            <ul>
                                <li><a href="#">PodElement 1</a></li>
                                <li><a href="#">PodElement 2</a></li>
                                <li><a href="#">PodElement 3</a></li>
                                <li><a href="#">PodElement 4</a></li>
                                <li><a href="#">PodElement 5</a></li>
                            </ul>
                        </li> 
                        <li><a href="http://google.pl">Element 4</a></li>
                        <li><a href="http://google.pl">Element 5</a></li>
                    </ul>
                </div>
                
            </div>
            

            <div id="content" class="container">
                
                <h1 class="title">Tytuł Strony</h1>
                
                <div id="leftColumn" class="column">
                    <ul class="tabs">
                        <li class="active"><a href="#" data-rel="tab1">Tab 1</a></li>
                        <li><a href="#" data-rel="tab2">Tab 2</a></li>
                        <li><a href="#" data-rel="tab3">Tab 3</a></li>
                        <li><a href="#" data-rel="tab4">Tab 4</a></li>
                        <li><a href="#" data-rel="tab5">Tab 5</a></li>
                    </ul>
                    <div id="tab1" class="tab active"><p>TAB 1</p><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam vestibulum dictum.Cras id erat pulvinar lacus porttitor dictum. Duis diam magna, tristique vitae nunc at, egestas malesuada arcu. Donec feugiat finibus ex eu condimentum. Sed vel erat pretium, ultrices ex cursus, interdum sapien.Donec feugiat finibus ex.</p></div>
                    <div id="tab2" class="tab"><p>TAB 2</p><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam vestibulum dictum.Cras id erat pulvinar lacus porttitor dictum. Duis diam magna, tristique vitae nunc at, egestas malesuada arcu. Donec feugiat finibus ex eu condimentum. Sed vel erat pretium, ultrices ex cursus, interdum sapien.Donec feugiat finibus ex.</p></div>
                    <div id="tab3" class="tab"><p>TAB 3</p><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam vestibulum dictum.Cras id erat pulvinar lacus porttitor dictum. Duis diam magna, tristique vitae nunc at, egestas malesuada arcu. Donec feugiat finibus ex eu condimentum. Sed vel erat pretium, ultrices ex cursus, interdum sapien.Donec feugiat finibus ex.</p></div>
                    <div id="tab4" class="tab"><p>TAB 4</p><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam vestibulum dictum.Cras id erat pulvinar lacus porttitor dictum. Duis diam magna, tristique vitae nunc at, egestas malesuada arcu. Donec feugiat finibus ex eu condimentum. Sed vel erat pretium, ultrices ex cursus, interdum sapien.Donec feugiat finibus ex.</p></div>
                    <div id="tab5" class="tab"><p>TAB 5</p><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla aliquam vestibulum dictum.Cras id erat pulvinar lacus porttitor dictum. Duis diam magna, tristique vitae nunc at, egestas malesuada arcu. Donec feugiat finibus ex eu condimentum. Sed vel erat pretium, ultrices ex cursus, interdum sapien.Donec feugiat finibus ex.</p></div>
                </div>
                
                <div id="rightColumn" class="column">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <div class="item"><img src="img/1.jpg" alt=""></div>
                        <div class="item"><img src="img/2.jpg" alt=""></div>
                        <div class="item"><img src="img/3.jpg" alt=""></div>
                    </div>
                </div>
                
            </div>
            
            
            <div id="footer" class="container">
                
                <div class="contactForm">
                    
                    <form method="POST">
                    
                        <div class="row">
                            <label for="subject">Temat:</label><br>
                            <input placeholder="Temat" id="subject" type="text" name="contact[subject]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="name">Imię i Nazwisko:</label><br>
                            <input placeholder="Imię i Nazwisko" id="name" type="text" name="contact[name]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="email">Email:</label><br>
                            <input placeholder="Email" id="email" type="text" name="contact[email]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="contactContent">Treść:</label><br>
                            <textarea id="contactContent" name="contact[content]" class="text" >Treść</textarea>
                        </div>

                        <div class="row submit">
                            <input id type="submit" class="button" value="Wyślij" />
                        </div>
                        
                    </form>
                    
                </div>
                
            </div>
            
            
        </div>
        
        
        <div id="slidingBox">
            <div class="label">
                &harr;
            </div>
            <div class="content">
                <a href="http://google.pl" target="_blank">Spytaj wujka Googla :)</a>
            </div>
        </div>
        
        
<!--        <div id="tester" class="aaa bbb ccc" data-rel="jakistamdodatek">
            <span>Tu jesteśmy</span>
        </div>
        -->
        
    </body>
    
</html>
