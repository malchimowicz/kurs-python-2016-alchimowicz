<?php

    require_once 'classes/class.registration.php';

    $registration = new REGISTRATION($_POST);
    $registration->showPostedData();
    
    if ($registration->isRegTry()) {
        
        if ($registration->formCheck()) {
            echo $registration->showMessage();    
            // Wyświetl Forma
        }
         
        else {
            echo $registration->showMessage();
            echo $registration->makeRecord();
        }
        
    }
    
    else {
        
        
        
    }
    

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Formularz</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <link rel="stylesheet" href="css/style.css" type="text/css" />
    
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>

<body>



<div class="container-fluid">
<div class="registerForm">
    
    <form method="POST" class="form-horizontal">
     
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Imię:</label>
            <div class="col-sm-3">
            <input id="name" name="register[name]" type="text" value="" class="text" />
        </div>
        </div>

        <div class="form-group">
            <label for="surname" class="col-sm-2 control-label">Nazwisko:</label>
            <div class="col-sm-3">
            <input id="surname" name="register[surname]" type="text" value="" class="text" />
        </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email:</label>
            <div class="col-sm-3">
            <input id="email" name="register[email]" type="emil" value="" class="text" />
        </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Hasło:</label>
            <div class="col-sm-3">
            <input id="password" name="register[password]" type="password" value="" class="text" />
        </div>
        </div>

        <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" value="Zarejestruj" class="btn btn-primary"" />
        </div>
        </div>
        
    </form>
    
</div>
</div>

</body>
</html>

