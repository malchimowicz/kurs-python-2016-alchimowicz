<?php


    // Klasa abstrakcyjna - nie można utworzyć jej instancji (obiektu)
    abstract class DEV {
        
        
        public static function wellPrint($elem, $type = False) {
        
            echo '<pre>';

            if ($type == 'p' || !$type) {
                print_r($elem);
            }
            else if ($type == 'v') {
                var_dump($elem);
            }

            echo '</pre>';

        }
        
        
        public static function createObjectFromFile($c) {
            $arr = explode('<br/>', $c);
            $data = new stdClass();
            foreach ($arr as $user) {
                $userData = explode(';', $user);
                if ($userData[2]) {
                    $data->{$userData[2]}->name = $userData[0];
                    $data->{$userData[2]}->surname = $userData[1];
                    $data->{$userData[2]}->password = $userData[3];
                }
            }
            return $data;
        }
        
        
    }