<?php


    class REGISTRATION {
        
        
        private $data;

        private $error = 0;
        private $message = 'Wystąpiły Błędy:<br/>';
        
        private $record;
        
        
        public function __construct($post) {
            $this->data = $post;
        }
        
        
        public function showPostedData() {
            return DEV::wellPrint($this->data);
        }
        
        
        public function isRegTry() {
            if (isset($this->data['register']) && $this->data['register'] != NULL) {
                return true;
            }
            else {
                return false;
            }
        }
        
        
        private function regValidation() {
            
            foreach ($this->data['register'] as $field => $value) {
                
                if ($field == 'name' && strlen($value) < 3) {
                    $this->error = 1;
                    $this->message .= 'Imię Powinno składać się z przynajmniej 3 znaków<br/>';
                }

                if ($field == 'surname' && strlen($value) < 3) {
                    $this->error = 1;
                    $this->message .= 'Nazwisko Powinno składać się z przynajmniej 3 znaków<br/>';
                }

                if ($field == 'email' && strlen($value) < 6) {
                    $this->error = 1;
                    $this->message .= 'Niepoprawny adres email';
                }

                if ($field == 'password' && strlen($value) < 5) {
                    $this->error = 1;
                    $this->message .= 'Hasło Powinno składać się z przynajmniej 3 znaków';
                }
                
            }
            
            return $this->error;
            
        }
        
        
        public function formCheck() {
            $this->regValidation();
            if ($this->error) {
                return true;
            }
            else {
                $this->message = 'Wszystko OK';
            }
            return false;
        }
        
        
        public function showMessage() {
            return $this->message;
        }
        
        
        public function createRecord() {
            $this->record = trim($this->data['register']['name']) . ';';
            $this->record .= trim($this->data['register']['surname']) . ';';
            $this->record .= trim($this->data['register']['email']) . ';';
            $this->record .= trim($this->data['register']['password']) . '<br/>';
            return $this->record;
        }
        
        
        public function makeRecord() {
            $myfile = fopen('_db/users.txt', 'a+');
            fwrite($myfile, nl2br($this->createRecord()));
//            $textContent = fread($myfile, filesize('_db/users.txt'));
            fclose($myfile);
        }
        
    }
