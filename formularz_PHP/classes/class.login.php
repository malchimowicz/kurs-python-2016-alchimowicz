<?php

    class LOGIN {
        
        
        private $data;
        
        private $loginData;
        
        private $ListOfUsers;
        
        
        public function __construct($data) {
            $this->data = $data;
            $this->setAndGetLoginData();
            $this->usersList();
        }
        
        
        private function setAndGetLoginData() {
            $this->loginData->login = $this->data['user'];
            $this->loginData->password = $this->data['password'];
            return $this->loginData;
        }
        
        
        private function usersList() {
            $myfile = fopen('_db/users.txt', 'r');
            $content = fread($myfile, filesize('_db/users.txt'));
            fclose($myfile);
            return $this->ListOfUsers = DEV::createObjectFromFile($content);
        }
        
        public function getUsersList() {
            return $this->ListOfUsers;
        }
        
        public function getLoginData() {
            return $this->loginData;
        }
        
        
    }
