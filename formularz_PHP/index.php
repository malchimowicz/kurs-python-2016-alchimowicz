<?php

    header('Content-Type: text/html; charset=utf-8'); // Deklaracja Kodowania Znaków
    
    // Metoda header musi być wywoływana przed wydrukiem jakiegokolwiek znaku
    
//    ini_set("display_errors", "1");
//    error_reporting(E_ALL);
    
    session_start(); // Inicjacja Sesji
    
    include_once 'classes/class.dev.php'; // M
    require_once 'include/globals.php';
    
//    DEV::wellPrint($_GET);
    
    if (isset($_GET['logout']) && $_GET['logout'] == 'true') {
        unset($_SESSION['USER']);
    }
    
    // Czy użytkownik zalogowany?
    if (isset($_SESSION['USER']) && $_SESSION['USER'] != NULL) { // Tak
        
        echo '<a href="index.php?logout=true">Wyloguj się</a>';
        
    }
    
    else { // Nie 
        
        // Czy nastąpiła próba logowania?
        if (isset($_POST['login']) && $_POST['login'] != NULL) { // Tak
                 
            require_once 'classes/class.login.php';
            
            $login = new LOGIN($_POST['login']);
            
            $loginEmail = $login->getLoginData()->login;
            $loginPassword = $login->getLoginData()->password;
            
            // Czy w bazie istnieje Użytkownik o takim mailu
            if ($loginEmail && $login->getUsersList()->{$loginEmail}) { // Tak
                
                $typCoSieLoguje = $login->getUsersList()->{$loginEmail};
                
                // Czy poprawne Hasło
                if ($typCoSieLoguje->password == $loginPassword) {
                    
                    ///////////////////////////////////////
                    
                    $_SESSION['USER']['email'] = $loginEmail;
                    $_SESSION['USER']['logged'] = time();
                    
                    header("HTTP/1.1 301 Moved Permanently"); 
                    header("Location: index.php"); 
                    
                    echo 'Siemano, zapraszmy na drineczka';
                    
                    ///////////////////////////////////////
              
                }
                
                else {
                    echo 'Podałeś błędne hasło głupcze';
                    include_once 'tpl/forms/login.php';
                }
                
            }
            else { // Nie
                echo 'Brak Adresu w bazie';
                include_once 'tpl/forms/login.php';
            }
            
        }
        
        else { // Nie (Nie było próby Logowania)
            include_once 'tpl/forms/login.php';
        }
        
    }
    
    
    include_once 'tpl/header.php';
    
    include_once 'tpl/content.php';
    
    include_once 'tpl/footer.php';
    
//    var_dump(file_exists('_db/users.txt'));
    


//    include_once '_db/users.txt'; 
    
    