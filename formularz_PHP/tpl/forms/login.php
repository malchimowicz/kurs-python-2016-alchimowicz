<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Logowanie</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style>
    body{
        background-image: url('img/hexellence.png');
    }
</style>

</head>

<body>


<div class="loginForm">
<div class="container-fluid">
    
    <form method="POST" class="form-horizontal">
        
        <div class="form-group">
            <label for="user" class="col-sm-2 control-label">Login:</label>
        <div class="col-sm-3">
            <input id="user" type="text" name="login[user]" value="" class="form-control"/>
        </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Hasło:</label>
        <div class="col-sm-3">
            <input id="password" type="password" name="login[password]" value="" class="form-control" />
            <span id="helpBlock" class="help-block">Login testowy: jan.kowal@gmail.com<br/> Hasło testowe: haslo123</span>
        </div>
        </div>

        <div class="form-group ">

        <div class="col-sm-offset-2 col-sm-10">
        <input type="submit" value="Zaloguj" class="btn btn-primary" />

        </div>

        </div>
        <p class="col-sm-offset-2" style='padding-left: 0'><a href="index.php?page=register" title="Zarejestruj się" >Zarejestruj się</a></p>
        <p class="col-sm-offset-2" style='padding-left: 0'><a href="index.php?page=history" title="Historia" >Historia</a></p>
        <p class="col-sm-offset-2" style='padding-left: 0'><a href="index.php?page=order" title="Zarówienia" >Zamówienia</a></p>
        <p class="col-sm-offset-2" style='padding-left: 0'><a href="index.php?page=profile" title="Profil" >Profil</a></p>

    </form>   
    
    
    
</div>
</div>

</body>
</html>


