$(document).ready(function(){
    var questions = [
        {
        "question":"ile czasu potrzeba, aby przejść z punktu A do punktu B?",
        "answers": ["5 godzin","2 lata","3 minuty", "Weź się, Hubert"],
        "correctAnswer": "2 lata",
        "tip": "Więcej niż 3 minuty."
        },
        {
        "question":"ile wynosi 2+2?",
        "answers": ["4","około 4","2+2 = 5", "1234567"],
        "correctAnswer": "około 4",
        "tip": "coś tam"
            
        },
        {
        "question":"Dlaczego szkolenie odbywa się w SW?",
        "answers": ["Brak prądu","duża sala","hipster", "Drogie jedzenie"],
        "correctAnswer": "Brak prądu",
        "tip": "Nie wiem."
        }
    ],
        usedIndexes = [], currentQuestion = getRandomQuestion(questions,usedIndexes);
    
function fillData(obj){
     getInputsUndisabled();
    
    $('.question').html(obj["question"]);
    obj["answers"] = shuffle(obj["answers"]);
    $('.answer').each(function(i,e){
        console.log(i,e);
        e.innerHTML = obj["answers"][i];
        
    });

}
    function getRandomQuestion(questions,usedIndexes){
        var index = getRandomIndex(questions, usedIndexes);
        if(index === undefined){
            winGame();
        }
        return questions[index];
    }
    
//    kolo ratunkowe
    function getHelpFromFriend(currentQuestion){
        alert(currentQuestion["tip"]);
        $('.friendHelp').attr('disabled','disabled');
        return currentQuestion["tip"]; 
    }
    
    $('.friendHelp').click(function(){
        getHelpFromFriend(currentQuestion);
    });
    
    
    $('.fiftyFiftyHelp').click(function(){
        getHelpFromComputer(currentQuestion);
    });
    
    $('.audienceHelp').click(function(){
        getHelpFromAudience(currentQuestion);
    });
    
    function  getHelpFromAudience(currentQuestion){
        var tempValues = [],sum = 0,tempPercent = [],str ="";
        for(var i = 0; i <4; i++){
            tempValues[i] = (Math.random() * 50) | 0;
            sum += tempValues[i];
        }
        for(var i = 0; i <4;i++){
            tempPercent[i] = (tempValues[i] * 100 / sum) | 0;
        }
        
        tempPercent = tempPercent.sort(function(a,b){return a- b});
        $('.answer').each(function(i,e){
            console.log(e.dataset.letter, (e.innerHTML == currentQuestion['correctAnswer']));
            if(e.innerHTML == currentQuestion['correctAnswer']){
               str += e.dataset.letter + ": " + tempPercent.pop() + "%\n";
            }else{
                str += e.dataset.letter + ": " + tempPercent.shift() + "%\n"; 
            }
        });
        alert(str);
        $('.audienceHelp').attr('disabled','disabled')
        
        console.log(tempValues);
        console.log(tempPercent.sort().pop());
    }
    
    
    function getHelpFromComputer(currentQuestion){
        var tempArr = [];
        console.log(currentQuestion['correctAnswer']);
        $('.answer').each(function(i,e){
            if(e.innerHTML != currentQuestion['correcAnswer']){
                tempArr.push(e);
            }
        });
        tempArr.pop().disabled ='disabled';
        tempArr.pop().disabled ='disabled';
        $('.fiftyFiftyHelp').attr('disabled','disabled');
    }
    
    function getInputsUndisabled(){
        $('.answer').each(function(i,e){
            $(this).removeAttr('disabled');
        });
    }
    
    
    function getRandomIndex(questions,usedIndexes){
        var elem = questions.length,
            tempArr=[];
        for(var i = 0; i < elem; i++){
            if(usedIndexes.indexOf(i) == -1) tempArr.push(i);
        }
        random = (Math.random()*tempArr.length) | 0;
        usedIndexes.push(tempArr[random]);
        return tempArr[random];
        
    }
    
//    f.konczonca gre
    function endGame(){
       
        $('.question, .answer').hide();
        alert("Przegrana")
    }
    
    
    function winGame(){
        $('.question, .answer').hide()
        alert("wygrana!")
    }
    
    function shuffle(arr){
	var rand,temp;
	for(var i = arr.length; i > 0; i--){
		rand = (Math.random() * i) | 0;
		temp = arr[i-1];
		arr[i -1] = arr[rand];
		arr[rand] = temp;
        console.log("wylosowalem do zamiany indeks " + rand + ".zamieniam aktualne iteracje " + i + " z indeksem " + rand);
	
	}
	return arr;
}
       
    
    $('.answer').click(function(){
        if($(this).html() == currentQuestion["correctAnswer"]){
            console.log("brawo");
            currentQuestion = getRandomQuestion(questions,usedIndexes);
            fillData(currentQuestion);
            
        }else{
            console.log('zle');
            endGame();
        
        
        };
    })
    
    
    fillData(currentQuestion);
    
//    console.log(questions[2]["question"]);
});
