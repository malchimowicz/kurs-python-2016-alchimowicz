$(document).ready(function() {
	var currentSymbol = 'O', usedIndexes = [];
	$('.col-md-4').click(function() {
		//console.log("Kliknieto w " + $(this).data('pos'));
		// 1. napiszmy kod, ktory wstawi w aktualnie kliknietego div'a currentSymbol,
		// 		1a. zabezpieczmy wstawianie k/k w miejsce, gdzie 
		// 		element istnieje - w tym celu, sprawdzmy czy
		//		aktualnie klikniety div jest pusty
		// 		jesli tak - mozemy umiescic symbol 
		// 		jesli nie - nie 
		var id = parseInt($(this).data('pos'));
		if(usedIndexes.indexOf(id) == -1) {
			$(this).html(currentSymbol);
			usedIndexes.push(id);
			// 2. a nastepnie zmieni currentSymbol na inny (X -> O, O -> X)
			checkGameStatus();
			currentSymbol = (currentSymbol == 'O') ? 'X' : 'O';
		}
			
	});
	
	function checkGameStatus() {
		// 1. sprawdzamy, czy gracz wygral
		var isWinner = checkForWin(); // var isWinner = [1, 2, 3];
		if(isWinner != false) {
			// kod realizujacy kolorowanie na zielono pól ze zwróconych indeksów
			for(var i = 0; i < isWinner.length; i++) {
				$('[data-pos=' + isWinner[i] + ']').css('background-color', 'lightgreen');
			}
			alert("WINNER IS PLAYER " + currentSymbol);
			resetVars();
		// 2. sprawdzamy czy jest remis
		} else if(usedIndexes.length == 9) {
			// remis
			alert("REMIS");
			resetVars();
		}
		
		// 3. w przeciwnym wypadku gracz przegral
	}
	
	function resetVars() {
		usedIndexes = [];
		$('.col-md-4').html("");
		for(var i = 0; i < 10; i++) {
			$('[data-pos=' + (i+1) + ']').css('background-color', 'white');
		}
	}
	
	function checkForWin() {
		var winPosibilities = [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9],
			[1, 4, 7],
			[2, 5, 8],
			[3, 6, 9],
			[1, 5, 9],
			[3, 5, 7]
		];
        for(var i = 0; i < winPosibilities.length; i++) {
			var currSearch = winPosibilities[i], elem = [], cs = currentSymbol;
			elem.push(  $('[data-pos=' + currSearch[0] + ']').html()  );
			elem.push(  $('[data-pos=' + currSearch[1] + ']').html()  );
			elem.push(  $('[data-pos=' + currSearch[2] + ']').html()  );
			//console.log(elem);
			if(elem[0] == cs && elem[1] == cs && elem[2] == cs) {
				return currSearch;
			}
		}
		return false;
	}
});