$(document).ready(function(){
    var wordsDictionary = ['oko','konik','hangar','python'],
    currentWord =  wordsDictionary[(Math.random() * wordsDictionary.length) | 0].toLocaleLowerCase(),
        currentUserWord = shadowWord(currentWord),
        usedLetter = [],
        tries = countLives(currentWord);
    console.log(currentWord);
    

    $('.wordCount').html(currentUserWord);
    $('.choicesLeft').html(tries);
    $('.letter').click(function(){
        
//        console.log($(this).val(),$(this).val().toLowerCase());
        var currentLetter = $(this).val().toLowerCase(),currentLetterIndex = [];
        usedLetter.push(currentLetter);
        $('.usedLetters').html(usedLetter.join(","));
        tries--;
        $('.choicesLeft').html(tries);
        for(var i = 0; i < currentWord.length; i++){
            if(currentWord[i] == currentLetter){
                console.log("Zgadnieto literki " + currentLetter + "pod indeksem "+i);
                currentLetterIndex.push(i);
            }
        }
        
        if(currentLetterIndex.length == 0){
            $(this).attr('disabled','disabled').css({'background-color':'#EBD0C7','border':'1px solid #cc0000',});
        }else{
            $(this).attr('disabled','disabled').css(
                {
                    'background-color':'green','border':'1px solid #cc0000',
                });
        }
        
        
        
       currentUserWord = unhideLetters(currentUserWord,currentLetter,currentLetterIndex);
        $('.wordCount').html(currentUserWord);
        
        if(currentUserWord == currentWord){
            showWinDialog();
        }
        if(tries == 0){
            showLostDialog();
        }
    });

    $('#guess').click(function(){
        if($('#guessedWord').val() == currentWord){
            showWinDialog();
        }
    })
});

    function shadowWord(str){
        return "_".repeat(str.length);
    }
   
function unhideLetters(sentence,letter,indexes){
    var tempArr = sentence.split("");
    if(indexes.length > 0){
    for(var i in indexes){
        tempArr[indexes[i]] = letter;
      }
    }
    return tempArr.join("");
}

function showWinDialog(){
    $('.wordCount').html('Wygrana' + '<br/><br/><button  class="reset btn btn-primary" onclick="location.reload()">Jeszcze raz?</button>').css('color','green');
    $('.guessBox').hide();
    $('.alphabet > input').hide();
}


function showLostDialog(){
     $('.wordCount').html('Przegrana' + '<br/><br/><button  class="reset btn btn-primary" onclick="location.reload()">Jeszcze raz?</button>').css('color','red');
    $('.guessBox').hide();
    $('.alphabet > input').hide();
}


function countLives(sentence){
    var tempArr = [], splitWord = sentence.split("")
    for(var i in sentence){
        if(tempArr.indexOf(splitWord[i]) == -1){
            tempArr.push(splitWord[i])
        }
    }
    
    return tempArr.length + 1;
}